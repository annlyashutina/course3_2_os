package ru.omsu.imit;

import ru.omsu.imit.exception.MyClientServerException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Semaphore;

/*Реализовать клиент-серверное приложение, в котором клиенты и сервер реализованы
в виде потоков.  Сервер-писатель считывает  строку со стандартного ввода и передает ее
через буфер клиентам. Клиенты-читатели считывают строку из буфера и записывают ее в
файл. Сервер заканчивает свою работу при получении со стандартного ввода строки «quit».
Клиенты заканчивают работу при чтении из буфера
данной строки. Для организации взаимного  исключения к
буферу и файлам использовать блокировки чтения записи.

    Один писатель, много читателей.
    Писатель читает с клавиатуры, пишет в буфер, читатели ждут.
     Писатель ждет, пока каждый читатель ровно 1 раз прочитает из буфера и запишет в файл
     * запуск из командной строки
     * вводим 1 параметр
     * количество читателей
     */

public class Main {

    public static void main(String[] args) throws MyClientServerException {
       int count = 0;

        if (args.length == 0) {
            throw new MyClientServerException("Введите количество читателей");
        }
        if (args.length == 1) {
             System.out.println("Необходимые параметры введены");
        }
        if (args.length > 1) {
            throw new MyClientServerException("Введено слишком много аргументов");
        }
            try {
                count = Integer.parseInt(args[0]);
                if (count < 1) {
                    throw new Exception();
                }
            } catch (Exception ex) {
                throw new MyClientServerException("Неверно введено количество читателей");
            }

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(new File(args[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }

        boolean[] forReaders = new boolean[count];
        for (int i=0; i<forReaders.length;i++){
            forReaders[i]=false;
        }

        StringBuffer bufferSTR = new StringBuffer();
        Buffer buffer = new Buffer(bufferSTR,forReaders);


        Semaphore canWrite = new Semaphore(count);
        Semaphore canRead = new Semaphore(0);


        ServerWriter serverWriter = new ServerWriter(canWrite, canRead, buffer, count);
        serverWriter.start();


        for (int i = 0; i < count; i++) {
            ClientReader clientReader = new ClientReader(i,fileWriter, buffer, canRead, canWrite);
            clientReader.start();
        }
    }
}
