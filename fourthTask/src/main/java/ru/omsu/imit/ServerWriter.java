package ru.omsu.imit;

import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class ServerWriter extends Thread {
    private Semaphore semaphoreWR;
    private Semaphore semaphoreRD;
    private Buffer buffer;
    private int countReaders;


    public ServerWriter(Semaphore semaphore, Semaphore semaphore1, Buffer buffer, int countReaders) {
        this.semaphoreWR = semaphore;
        this.semaphoreRD = semaphore1;
        this.buffer = buffer;
        this.countReaders = countReaders;

    }

    @Override
    public void run() {

        Scanner in = new Scanner(System.in);
        boolean[] forReaders = new boolean[countReaders];
        Boolean flag = false;
        while (!flag) {
            try {

                semaphoreWR.acquire(1);
                System.out.println("Сервер-писатель готов");
                System.out.println("Введите сообщение");
                String read = in.nextLine();
                buffer.setBuffer(buffer.getBuffer().delete(0, buffer.getBuffer().length()));
                buffer.setBuffer(buffer.getBuffer().append(read));

                if (read.equals("quit")) {
                    System.out.println("Сервер закончил свою работу");
                    semaphoreRD.release(countReaders);
                    flag = true;
                }

                for (int i=0; i<forReaders.length;i++){
                    forReaders[i]=false;
                }
                buffer.setForReaders(forReaders);

                semaphoreRD.release(1);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}