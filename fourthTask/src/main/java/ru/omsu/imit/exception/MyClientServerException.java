package ru.omsu.imit.exception;

public class MyClientServerException extends Exception {

    public MyClientServerException(String s) {
        super(s);
        System.out.println(s);
    }

}
