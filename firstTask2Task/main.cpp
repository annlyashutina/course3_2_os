#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>

#define FILE_NAME "file.hole"

using namespace std;

string generateData(int size) {
    string base = "a";

    if (size <= 0) {
        return "";
    }

    if (size == 1) {
        return base;
    }

    string result;
    for (int i = 0; i < size; i++) {
        result += base;
    }
    //string result = base + base;
    return result;
}

int main(int argc, char *argv[]) {

    //creating file
    int fd = creat(FILE_NAME, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (fd < 0) {
        printf("ошибка вызова creat");
        return -1;
    }
//Измените программу так, чтобы вся конфигурация файла
//(размеры чередуемых данных и дыр) передавалась через
//параметры командной строки.  Дыр в файле может быть
//сколько угодно.

//Поскольку дыр в файле может быть сколько угодно, от 0 до бесконечности, число дырок не будет влиять на минимальное число аргументов в командной строке
//Про количество данных в файле ничего не указано, будем считать, что в файле должен быть минимум один блок с данными или с дыркой
//Первым аргументом всегда неявно передаётся путь к исполняемой программе
//Таким образом у нас должно быть не меньше 2 аргументов (один неявный и один явный)
    if(argc < 2) {
        printf("Должен быть минимум один аргумент командной строки\n");
    }

    for (int i = 1; i < argc; i++) {
        long dataSize = atol(argv[i]);
        if (!dataSize)
        {
            printf("%s , не является числом", argv[i]);
        }

        if (dataSize == 0) {
            continue;
        }

        // If number negative, make a hole of length minus the number
        if (dataSize < 0) {
            if (lseek(fd, -dataSize, SEEK_CUR) == -1) {
                printf("ошибка вызова lseek");
                return 1;
            }
            continue;
        }

        //If the number is positive, generateData length the number
        while (dataSize > 0) {

            string data = generateData(dataSize);
            char *dataArray = &data[0];

            if (write(fd, dataArray, dataSize) != dataSize) {
                printf("ошибка записи dataArray");
                return 1;
            }
            break;
        }
    }

    printf("finished writing file");
    if (close(fd))
        printf("Error in closing file.\n");
    return 0;
}