#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("The command line must contain :path to the executable file by default and buffer size explicit\n"
               "Read from STDIN, writes to STDOUT\n");
        return 0;
    }

    ssize_t buff_size = atol(argv[1]);
    if (buff_size <= 0) {
        printf("The buffer size must be a positive number");
        return -1;
    }

    char *buff = malloc(buff_size);
    int ret = 0;

    ssize_t was_read = 0;
    do {
        was_read = read(STDIN_FILENO, buff, buff_size);

        if (was_read == -1) {
            perror("read() error");
            ret = 1;
            break;
        }

        if (was_read != 0 && write(STDOUT_FILENO, buff, was_read) != was_read) {
            perror("write() error");
            ret = 2;
            break;
        }
    } while (was_read == buff_size);

    free(buff);
    return ret;
}