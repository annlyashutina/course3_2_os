/**
 * Задание 3.1
 *
 * Считает число попавшихся по пути к корню ФС символических ссылок на
 * исходную рабочуюю директорию. Выводит на экран это число и путь к директории.
 */

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define exit_if(expr, msg) ((expr) ? (perror(msg), exit(EXIT_FAILURE)) : 0)

/// Элемент стэка имён файлов на основе односвязного списка
struct path_node {
    char *name;
    size_t size;
    struct path_node *next;
};

char *my_getcwd(size_t *links) {
    struct stat cur_stat, root_stat, cwd_stat, prev_stat;
    size_t path_length = 0;
    struct path_node *stack = NULL;
    struct dirent *dir;
    DIR *dp;
    *links = 0;

    // Заранее получаем информацию о текущем и корневом каталогах, чтобы потом сравнивать
    exit_if(stat("/", &root_stat), "Невозможно получить доступ к корневой статистике");
    exit_if(stat(".", &cwd_stat),
            "Невозможно получить доступ к текущей статистике рабочего каталога");

    do {
        // Открываем текущий каталог
        exit_if((dp = opendir(".")) == NULL, "Невозможно получить содержимое каталога");

        // Проходимся по каждому файлу текущего каталога
        errno = 0;
        while ((dir = readdir(dp)) != NULL) {
            exit_if(lstat(dir->d_name, &cur_stat), "Невозможно получить статистику файлов");

            // Если очередной файл является смволической ссылкой на что-то
            if (S_ISLNK(cur_stat.st_mode)) {
                // Получаем информацию о файле, на который указывала ссылка
                if (stat(dir->d_name, &cur_stat) == 0 && cur_stat.st_ino == cwd_stat.st_ino && cur_stat.st_dev == cwd_stat.st_dev) {
                    // Если файл, на который указывала ссылка - начальная рабочая директория
                    (*links)++;
                }
            } else if (cur_stat.st_ino == prev_stat.st_ino && cur_stat.st_dev == prev_stat.st_dev) {
                // Когда находим предыдущий каталог, запоминаем его имя в стэке
                size_t len = strlen(dir->d_name);
                struct path_node *node = malloc(sizeof(struct path_node));
                assert(node != NULL);

                node->next = stack;
                node->size = len;
                node->name = malloc(len);
                assert(node->name != NULL && len != 0);
                strncpy(node->name, dir->d_name, len);

                path_length = path_length + len + 1;  // +1 с запасом на /
                stack = node;
            }

            errno = 0;
        }

        exit_if(errno, "Невозможно получить содержимое каталога");

        exit_if(stat(".", &prev_stat), "Невозможно получить статистику каталога");

        exit_if(chdir(".."), "Невозможно изменить текущий рабочий каталог");
    } while (root_stat.st_ino != prev_stat.st_ino || root_stat.st_dev != prev_stat.st_dev);

    // Формируем строку пути
    char *path = malloc(path_length + 1);
    assert(path != NULL);
    size_t string_pos = 0;

    while (stack != NULL && string_pos + stack->size < path_length) {
        path[string_pos++] = '/';
        strncpy(path + string_pos, stack->name, stack->size);
        string_pos += stack->size;

        // Удаляем элемент стэка
        struct path_node *next = stack->next;
        free(stack->name);
        free(stack);
        stack = next;
    }

    path[string_pos] = '\0';
    return path;
}

int main(void) {
    size_t links;
    char *path = my_getcwd(&links);
    if (path == NULL) {
        return -1;
    }
    printf("%d\t%s\n", links, path);
    free(path);
    return 0;
}