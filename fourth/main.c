#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <string.h>

#define NUMBER_CLIENTS_READER 3

char buf[256] = "";
int clientRead = 1;
int currentClient = 0;
FILE* fileResult;

pthread_rwlock_t lockBuffer;
pthread_rwlock_t isReadLock;
pthread_rwlock_t lockFile;
pthread_rwlock_t lockCurrentClient;

void *serverWriter(void *argp) {
    for(;;) {
        pthread_rwlock_wrlock(&isReadLock);
        if (clientRead != 0) {
            pthread_rwlock_wrlock(&lockBuffer);
            fgets(buf, 256, stdin);
            if (feof(stdin) || strcmp("quit\n", buf) == 0) {
                clientRead = -1;
                pthread_rwlock_unlock(&lockBuffer);
                pthread_rwlock_unlock(&isReadLock);
                break;
            }
            clientRead = 0;
            pthread_rwlock_unlock(&lockBuffer);
            pthread_rwlock_wrlock(&lockCurrentClient); // Добавила блокировку на currentClient
            currentClient++;
            if (currentClient > NUMBER_CLIENTS_READER) {
                currentClient = 1;
            }
            printf("Записано в буфер (%d) %s", currentClient, buf);
            pthread_rwlock_unlock(&lockCurrentClient);
        }
        pthread_rwlock_unlock(&isReadLock);
        sched_yield();
    }
    puts("Сервер закончил свою работу");
    return (void *) 0;
}

void *clientReader(void *argp) {
    int index = *((int *) argp);
    printf("Клинт-читатель №%d\n", index);
    for(;;) {
        pthread_rwlock_wrlock(&isReadLock);
        if (clientRead == -1) {
            pthread_rwlock_unlock(&isReadLock);
            break;
        }
        if (currentClient == index && clientRead == 0) {
            pthread_rwlock_rdlock(&lockBuffer);
            pthread_rwlock_wrlock(&lockFile);
            fputs(buf, fileResult);
            pthread_rwlock_unlock(&lockFile);
            pthread_rwlock_unlock(&lockBuffer);
            clientRead = 1;
        }
        pthread_rwlock_unlock(&isReadLock);
        sched_yield();
    }
    printf("Закончил свою работу Клинт-читатель №%d\n", index);
    return (void *) 0;
}

int main() {
    pthread_t server_thread, client_threads[NUMBER_CLIENTS_READER];
    int client_numbers[NUMBER_CLIENTS_READER];
    fileResult = fopen("fileResult.txt", "w+");

    pthread_rwlock_init(&lockBuffer, 0);
    pthread_rwlock_init(&isReadLock, 0);
    pthread_rwlock_init(&lockFile, 0);
    pthread_rwlock_init(&lockCurrentClient, 0);

    pthread_create(&server_thread, 0, serverWriter, 0);
    for (int i = 0; i < NUMBER_CLIENTS_READER; i++) {
        client_numbers[i] = i + 1;
        pthread_create(&(client_threads[i]), 0, clientReader, (void *) &(client_numbers[i]));
    }

    pthread_join(server_thread, 0);
    for (int i = 0; i < NUMBER_CLIENTS_READER; i++) {
        pthread_join(client_threads[i], 0);
    }

    pthread_rwlock_destroy(&lockBuffer);
    pthread_rwlock_destroy(&isReadLock);
    pthread_rwlock_destroy(&lockFile);
    pthread_rwlock_destroy(&lockCurrentClient);
    fclose(fileResult);
    return 0;
}